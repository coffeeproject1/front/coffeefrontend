import type { Role } from './Role'
type Gender = 'male' | 'female' | 'others'
type User = {
  timein: any
  timein: any
  timeout: string
  id?: number
  email: string
  password: string
  fullName: string
  gender: Gender // Male, Female, Others
  image: string
  position: string
  roles: Role[] // admin, user
}
export type { Gender, User }
