// types/PaymentEntry.ts

export interface PaymentEntry {
  id: string
  date: string
  userId: string
  workingHours: number
  workRate: number
  action: string
}
