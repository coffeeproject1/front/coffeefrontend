type Salary = {
  id?: number
  date: string
  userid: number
  fullname: string
  workinghour: number
  workrate: number
  salary: number
}

export type { Salary }
