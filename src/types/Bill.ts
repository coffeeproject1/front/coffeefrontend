type Bill = {
  id?: number
  date: string
  invoice: string
  namepd: string
  amountbill: number
  units: string
  priceperunits: number
  discount: number
  price: number
  time: string
  timepay: string
  status: string
  isChecked: boolean
}

export type { Bill }
