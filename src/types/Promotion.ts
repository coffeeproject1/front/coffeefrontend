type Promotion = {
  id: number
  name: string
  discount: number
}

export { type Promotion }
