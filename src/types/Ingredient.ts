import type { Type } from './Type'

type Ingredient = {
  id?: number
  name: string
  price: number
  amount: number
  image: string
}
function getImageUrl(ingredient: Ingredient) {
  return `/img/coffees/ingredient${ingredient.id}.png`
}
export { type Ingredient, getImageUrl }
