import type { OrderItem } from './OrderItem'

type Order = {
  id?: number
  Date: string
  Amount: number
  Total_price: number
  Vendor: String
  OrderItems?: OrderItem[]
}

export type { Order }
