type Branch = {
  id: number
  name: string
  tel: string
}

export { type Branch }
