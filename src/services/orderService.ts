import type { Order } from '@/types/Order'
import http from './http'

function addOrder(order: Order & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', order.Date)
  formData.append('amount', order.Amount.toString())
  formData.append('totalprice', order.Total_price.toString())
  formData.append('vendor', order.Vendor.toString())
  if (order.files && order.files.length > 0) formData.append('file', order.files[0])
  return http.post('/orders', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateOrder(order: Order & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', order.Date)
  formData.append('amount', order.Amount.toString())
  formData.append('totalprice', order.Total_price.toString())
  formData.append('vendor', order.Vendor.toString())
  if (order.files && order.files.length > 0) formData.append('file', order.files[0])
  return http.post('/orders', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delOrder(order: Order) {
  return http.delete(`/orders/${order.id}`)
}

function getOrder(id: number) {
  return http.get(`/orders/${id}`)
}

function getOrders() {
  return http.get('/orders')
}

export default { addOrder, updateOrder, delOrder, getOrder, getOrders }
