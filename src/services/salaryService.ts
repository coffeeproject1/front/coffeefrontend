import type { Salary } from '@/types/Salary'
import http from './http'

function addSalary(salary: Salary) {
  return http.post('/Salarys', salary)
}

function updateSalary(salary: Salary) {
  return http.patch('/Salarys/' + salary.id, salary)
}

function delSalary(salary: Salary) {
  return http.delete('/Salarys/' + salary.id)
}

function getSalary(id: number) {
  return http.get('/Salarys/' + id)
}

function getSalarys() {
  return http.get('/Salarys')
}

function getSalarysPaid() {
  return http.get('/Salarys')
}

export default { addSalary, updateSalary, delSalary, getSalary, getSalarys, getSalarysPaid }
