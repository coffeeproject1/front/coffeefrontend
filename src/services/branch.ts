import type { Branch } from '@/types/Branch'
import http from './http'

function addBranch(branch: Branch & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', branch.name)
  formData.append('tel', branch.tel)
  if (branch.files && branch.files.length > 0) formData.append('file', branch.files[0])
  return http.post('/branchs', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateBranch(branch: Branch & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', branch.name)
  formData.append('tel', branch.tel)
  if (branch.files && branch.files.length > 0) formData.append('file', branch.files[0])
  return http.post(`/branchs/${branch.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delBranch(branch: Branch) {
  return http.delete(`/branchs/${branch.id}`)
}

function getBranch(id: number) {
  return http.get(`/branchs/${id}`)
}

function getBranchs() {
  return http.get('/branchs')
}

export default { addBranch, updateBranch, delBranch, getBranch, getBranchs }
