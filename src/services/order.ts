import type { Receipt } from '@/types/Receipt'
import http from './http'
import type { ReceiptItem } from '@/types/ReceiptItem'
type ReceiptDto = {
  orderItems: {
    productId: number
    qty: number
  }[]
  userId: number
}
function addOrder(receipt: Receipt, receiptItems: ReceiptItem[]) {
  const ReceiptDto: ReceiptDto = {
    orderItems: [],
    userId: 0
  }
  ReceiptDto.userId = receipt.userId
  ReceiptDto.orderItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      qty: item.unit
    }
  })

  return http.post('/orders', ReceiptDto)
}
export default { addOrder }
