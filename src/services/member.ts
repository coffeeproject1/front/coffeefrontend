import type { Member } from '@/types/Member'
import http from './http'

function addMember(member: Member & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', member.name)
  formData.append('tel', member.tel)
  if (member.files && member.files.length > 0) formData.append('file', member.files[0])
  return http.post('/members', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateMember(member: Member & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', member.name)
  formData.append('tel', member.tel)
  if (member.files && member.files.length > 0) formData.append('file', member.files[0])
  return http.post(`/members/${member.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delMember(member: Member) {
  return http.delete(`/members/${member.id}`)
}

function getMember(id: number) {
  return http.get(`/members/${id}`)
}

function getMembers() {
  return http.get('/members')
}

export default { addMember, updateMember, delMember, getMember, getMembers }
