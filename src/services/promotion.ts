import type { Promotion } from '@/types/Promotion'
import http from './http'

function addPromotion(promotion: Promotion & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', promotion.name)
  formData.append('discount', promotion.discount.toString())
  if (promotion.files && promotion.files.length > 0) formData.append('file', promotion.files[0])
  return http.post('/promotions', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updatePromotion(promotion: Promotion & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', promotion.name)
  formData.append('discount', promotion.discount.toString())
  if (promotion.files && promotion.files.length > 0) formData.append('file', promotion.files[0])
  return http.post(`/promotions/${promotion.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delPromotion(promotion: Promotion) {
  return http.delete(`/promotions/${promotion.id}`)
}

function getPromotion(id: number) {
  return http.get(`/promotions/${id}`)
}

function getPromotions() {
  return http.get('/promotions')
}

export default { addPromotion, updatePromotion, delPromotion, getPromotion, getPromotions }
