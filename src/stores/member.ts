import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import memberService from '@/services/member'
import type { Member } from '@/types/Member'
import { useMessageStore } from './message'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const members = ref<Member[]>([])
  const initialMember: Member = {
    id: 0,
    name: '',
    tel: ''
  }
  const editedMember = ref<Member & { files: File[] }>(JSON.parse(JSON.stringify(initialMember)))

  async function getMember(id: number) {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMember(id)
      editedMember.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('eror')
      loadingStore.finish()
    }
  }
  async function getMembers() {
    loadingStore.doLoad()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()
  }
  async function saveMember() {
    try {
      loadingStore.doLoad()
      const member = editedMember.value
      if (!member.id) {
        // Add new
        console.log('Post ' + JSON.stringify(member))
        const res = await memberService.addMember(member)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(member))
        const res = await memberService.updateMember(member)
      }
      await getMembers()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteMember() {
    try {
      loadingStore.doLoad()
      const member = editedMember.value
      const res = await memberService.delMember(member)
      await getMembers()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedMember.value = JSON.parse(JSON.stringify(initialMember))
  }
  return { members, getMembers, saveMember, deleteMember, editedMember, getMember, clearForm }
})
