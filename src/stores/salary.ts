import { ref, computed, nextTick, onMounted, type Ref, watchEffect } from 'vue'
import { defineStore } from 'pinia'
import salaryService from '@/services/salaryService'
import type { Salary } from '@/types/Salary'
import { useLoadingStore } from '@/stores/loading'

const loadingStore = useLoadingStore()
const loading = ref(false)
const paymentDialog1 = ref(false)
const paymentDialog2 = ref(false)
const salarys = ref<Salary[]>([])
const salarysPaid = ref<Salary[]>([])
const currentTime = ref<string>('')

export const useSalaryStore = defineStore('salary', () => {
  const initilSalary: Salary = {
    date: '',
    userid: 0,
    fullname: '',
    workinghour: 0,
    workrate: 0,
    salary: 0
  }
  const editedSalary = ref<Salary>(JSON.parse(JSON.stringify(initilSalary)))

  async function getSalarys() {
    loadingStore.doLoad()
    const res = await salaryService.getSalarys()
    salarys.value = res.data
    loadingStore.finish()
  }

  async function getSalary(id: number) {
    loadingStore.doLoad()
    const res = await salaryService.getSalary(id)
    editedSalary.value = res.data
    loadingStore.finish()
  }

  async function saveSalary() {
    const Salary = editedSalary.value
    loadingStore.doLoad()
    if (!Salary.id) {
      //Add new
      editedSalary.value.salary = editedSalary.value.workinghour * editedSalary.value.workrate
      const res = await salaryService.addSalary(Salary)
    } else {
      //Update
      const res = await salaryService.updateSalary(Salary)
    }
    await getSalarys()
    loadingStore.finish()
  }

  async function deleteSalary() {
    loadingStore.doLoad()
    const res = await salaryService.delSalary(editedSalary.value)
    await getSalarys()
    loadingStore.finish()
  }

  function clearForm() {
    editedSalary.value = JSON.parse(JSON.stringify(initilSalary))
  }
  async function handleClosePaymentDialog(item: Salary) {
    loadingStore.finish()
    paymentDialog1.value = false
    paymentDialog2.value = false
    await getSalarys()
  }

  onMounted(() => {})

  return {
    salarys,
    salarysPaid,
    getSalarys,
    saveSalary,
    deleteSalary,
    clearForm,
    currentTime,
    handleClosePaymentDialog,
    editedSalary,
    getSalary,
    paymentDialog1,
    paymentDialog2
  }
})
