import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import ingredientService from '@/services/ingredientService'
import type { Ingredient } from '@/types/Ingredient'
import { useMessageStore } from './message'

export const useIngredientStore = defineStore('ingredient', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const ingredients = ref<Ingredient[]>([])
  const initialIngredient: Ingredient & { files: File[] } = {
    name: '',
    price: 0,
    image: 'noimage.jpg',
    amount: 0,
    files: []
  }
  const editedIngredient = ref<Ingredient & { files: File[] }>(
    JSON.parse(JSON.stringify(initialIngredient))
  )

  async function getIngredient(id: number) {
    try {
      loadingStore.doLoad()
      const res = await ingredientService.getIngredient(id)
      editedIngredient.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      console.log('eror')
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function getIngredients() {
    loadingStore.doLoad()
    const res = await ingredientService.getIngredients()
    ingredients.value = res.data
    loadingStore.finish()
  }
  async function saveIngredient() {
    try {
      loadingStore.doLoad()
      const ingredient = editedIngredient.value
      if (!ingredient.id) {
        // Add new
        console.log('Post ' + JSON.stringify(ingredient))
        const res = await ingredientService.addIngredient(ingredient)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(ingredient))
        const res = await ingredientService.updateIngredient(ingredient)
      }
      await getIngredients()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteIngredient() {
    try {
      loadingStore.doLoad()
      const ingredient = editedIngredient.value
      const res = await ingredientService.delIngredient(ingredient)
      await getIngredients()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedIngredient.value = JSON.parse(JSON.stringify(initialIngredient))
  }
  return {
    ingredients,
    getIngredients,
    saveIngredient,
    deleteIngredient,
    editedIngredient,
    getIngredient,
    clearForm
  }
})
