import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import branchService from '@/services/branch'
import type { Branch } from '@/types/Branch'
import { useMessageStore } from './message'

export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const branchs = ref<Branch[]>([])
  const initialBranch: Branch = {
    id: 0,
    name: '',
    tel: ''
  }
  const editedBranch = ref<Branch & { files: File[] }>(JSON.parse(JSON.stringify(initialBranch)))

  async function getBranch(id: number) {
    try {
      loadingStore.doLoad()
      const res = await branchService.getBranch(id)
      editedBranch.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('eror')
      loadingStore.finish()
    }
  }
  async function getBranchs() {
    loadingStore.doLoad()
    const res = await branchService.getBranchs()
    branchs.value = res.data
    loadingStore.finish()
  }
  async function saveBranch() {
    try {
      loadingStore.doLoad()
      const branch = editedBranch.value
      if (!branch.id) {
        // Add new
        console.log('Post ' + JSON.stringify(branch))
        const res = await branchService.addBranch(branch)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(branch))
        const res = await branchService.updateBranch(branch)
      }
      await getBranchs()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteBranch() {
    try {
      loadingStore.doLoad()
      const branch = editedBranch.value
      const res = await branchService.delBranch(branch)
      await getBranchs()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
  }
  return { branchs, getBranchs, saveBranch, deleteBranch, editedBranch, getBranch, clearForm }
})
