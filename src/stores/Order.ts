import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Order } from '@/types/Order'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import orderService from '@/services/orderService'

export const useOrderStore = defineStore('order', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const orders = ref<Order[]>([])
  const initialOrder: Order & { files: File[] } = {
    Date: '',
    Amount: 0,
    Total_price: 0,
    Vendor: '',
    files: []
  }
  const editedOrder = ref<Order & { files: File[] }>(JSON.parse(JSON.stringify(initialOrder)))

  async function getOrder(id: number) {
    try {
      loadingStore.doLoad()
      const res = await orderService.getOrder(id)
      editedOrder.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      console.log('error')
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function getOrders() {
    loadingStore.doLoad()
    const res = await orderService.getOrders()
    orders.value = res.data
    loadingStore.finish()
  }
  async function saveOrder() {
    try {
      loadingStore.doLoad()
      const order = editedOrder.value
      if (!order.id) {
        // Add new
        console.log('Post ' + JSON.stringify(order))
        const res = await orderService.addOrder(order)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(order))
        const res = await orderService.updateOrder(order)
      }
      await getOrders()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteOrder() {
    try {
      loadingStore.doLoad()
      const order = editedOrder.value
      const res = await orderService.delOrder(order)
      await getOrders()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedOrder.value = JSON.parse(JSON.stringify(initialOrder))
  }
  return {
    orders,
    getOrders,
    saveOrder,
    deleteOrder,
    editedOrder,
    getOrder,
    clearForm
  }
})
