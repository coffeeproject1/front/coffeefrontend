import { ref, computed, nextTick, onMounted, type Ref, watchEffect } from 'vue'
import { defineStore } from 'pinia'
import billService from '@/services/billService'
import type { Bill } from '@/types/Bill'
import { useLoadingStore } from '@/stores/loading'

const loadingStore = useLoadingStore()
const loading = ref(false)
const paymentDialog1 = ref(false)
const paymentDialog2 = ref(false)
const bills = ref<Bill[]>([])
const isPaid = (item: Bill) => {
  return item.timepay !== 'null'
}
const currentTime = ref<string>('')

export const useBillStore = defineStore('bill', () => {
  const initilBill: Bill = {
    invoice: 'P',
    date: '',
    namepd: '',
    amountbill: 0,
    units: '',
    priceperunits: 0,
    discount: 0,
    price: 0,
    time: currentTime.value,
    timepay: 'null',
    status: '',
    isChecked: false
  }
  const editedBill = ref<Bill>(JSON.parse(JSON.stringify(initilBill)))

  async function getBills() {
    loadingStore.doLoad()
    const res = await billService.getBills()
    bills.value = res.data
    loadingStore.finish()
  }

  async function getBill(id: number) {
    loadingStore.doLoad()
    const res = await billService.getBill(id)
    editedBill.value = res.data
    loadingStore.finish()
  }

  async function saveBill() {
    const Bill = editedBill.value
    loadingStore.doLoad()
    if (!Bill.id) {
      //Add new
      editedBill.value.price =
        editedBill.value.amountbill * editedBill.value.priceperunits - editedBill.value.discount
      Bill.time = getCurrentTime()
      const res = await billService.addBill(Bill)
    } else {
      //Update
      const res = await billService.updateBill(Bill)
    }
    await getBills()
    loadingStore.finish()
  }

  async function deleteBill() {
    loadingStore.doLoad()
    const res = await billService.delBill(editedBill.value)
    await getBills()
    loadingStore.finish()
  }

  function clearForm() {
    editedBill.value = JSON.parse(JSON.stringify(initilBill))
  }

  async function pay(item: Bill) {
    loadingStore.doLoad()
    if (!item.timepay || item.timepay === 'null') {
      item.timepay = getCurrentTime()
      item.status = 'Paid'
      await billService.updateBill(item)
      paymentDialog1.value = true
      loadingStore.finish()
    }
  }

  async function cancelPayment(item: Bill) {
    loadingStore.doLoad()
    if (isPaid(item)) {
      item.timepay = 'null'
      item.status = 'Un-Paid'
      item.isChecked = false
      await billService.updateBill(item)
      paymentDialog2.value = true
      loadingStore.finish()
    }
  }
  async function handleClosePaymentDialog(item: Bill) {
    loadingStore.finish()
    paymentDialog1.value = false
    paymentDialog2.value = false
    item.isChecked = true
    await getBills()
  }

  function getCurrentTime() {
    const now = new Date()
    const formatter = new Intl.DateTimeFormat('en', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    })
    return formatter.format(now)
  }

  onMounted(() => {
    currentTime.value = getCurrentTime()
  })

  return {
    bills,
    getBills,
    saveBill,
    deleteBill,
    clearForm,
    currentTime,
    getCurrentTime,
    editedBill,
    getBill,
    pay,
    cancelPayment,
    paymentDialog1,
    paymentDialog2,
    handleClosePaymentDialog
  }
})
